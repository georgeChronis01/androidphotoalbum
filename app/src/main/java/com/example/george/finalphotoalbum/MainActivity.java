package com.example.george.finalphotoalbum;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import static android.util.Log.VERBOSE;
import static com.example.george.finalphotoalbum.R.drawable.bg_key;
import static com.example.george.finalphotoalbum.photoApp.readApp;

public class MainActivity extends AppCompatActivity implements Serializable{
    private transient Button openAlbumButton;
    private transient Button deleteAlbumButton;
    private transient Button renameAlbumButton;
    private transient Button addAlbumButton;
    private transient Button searchButton;
    private transient ListView albumList;
    public static ArrayList<Album> albums;
    public  ArrayList<Album> serialAlbums;
    private transient String newName="";
    private transient Album newAlbum;
    public static Album currAlbum;
    private  transient ArrayAdapter<Album> thisAdapter;
    public static photoApp thisApp;
    private static final String TAG = "SampleActivity";


    private static final boolean VERBOSE = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //have to instantiate the photoApp if and if readApp is successful
        //then set it to that
        System.out.println("ON CREATE");
        thisApp = new photoApp();
//        try {
//            photoApp.writeApp(this, thisApp);
//        } catch (IOException e) {
//            System.out.print("writing to create file didn't work");
//            e.printStackTrace();
//        }
        try {
            thisApp=readApp(this);
        }
        catch(Exception e){
            System.out.print("reading from serialization didn't work");
            e.printStackTrace();
        }
        //after reading from serialize
        albums=thisApp.albums;
        System.out.println(thisApp.albums.size());
       // albums=serialAlbums;
//        Album one = new Album("one");
//        Photo first = new Photo(R.drawable.black_bishop);
//        Photo second = new Photo(R.drawable.black_rook);
//        Photo third = new Photo(R.drawable.black_king);
////        Photo fourth = new Photo(R.drawable.betterchessboard);
//
//        one.addPhoto(first);
//        one.addPhoto(second);
//        one.addPhoto(third);
////        one.addPhoto(fourth);
//        System.out.println("size "+one.getPhotos().size());
//        Album two = new Album("two");
//        Album three = new Album("three");
//        Album four = new Album("four");
//        Album five = new Album("five");
//        albums = new ArrayList<Album>();
//        albums.add(one);
//        albums.add(two);
//        albums.add(three);
//        albums.add(four);
//        albums.add(five);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openAlbumButton = (Button)findViewById(R.id.openAlbumButton);
        deleteAlbumButton = (Button)findViewById(R.id.deleteAlbumButton);
        renameAlbumButton = (Button)findViewById(R.id.renameAlbumButton);
        searchButton = (Button)findViewById(R.id.searchButton);
        addAlbumButton= (Button)findViewById(R.id.addAlbumButton);
        albumList=(ListView) findViewById(R.id.albumList);
        albumList.setSelector(android.R.color.darker_gray);
        //albumList.setSelector(bg_key);
        albumList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        albumList.setItemChecked(0, true);
        albumList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                //view.setSelected(true);
                //trying to highlight the selected item ....
                albumList.setItemChecked( 0, true );
                currAlbum=(Album)albumList.getItemAtPosition(position);
                System.out.println(currAlbum);
                System.out.println();
            }
        });

        //open album
        openAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(currAlbum!=null) {
                        //open thumbnail view of this album's photos
                        Intent intent = new Intent(MainActivity.this, ThumbnailActivity.class);
                        //probably have to pass the current album in a bundle
                        Bundle bundle = new Bundle();

                        //bundle.putSerializable("current album", currAlbum);
                        bundle.putString("activity","main");
                        //System.out.println("===================" + currAlbum);
                        intent.putExtras(bundle);

                        //intent.putExtra("name_of_extra", (Serializable)currAlbum);
                        startActivity(intent);
                        //MainActivity.this.startActivity(intent);
                    }
                    else{
                        System.out.println("NOTHING SELECTED!!!!!!!!!!!!!!!!");
                    }

                } catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                }
            }
        });
        deleteAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //get selected album
                    deleteAlbum(currAlbum);
                    thisAdapter.notifyDataSetChanged();
                    //and delete from the list of albums and reload the list?
                   // albumList.setAdapter(adapter);
                } catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                }
            }
        });
        renameAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //open dialog to input new name

                    //ONLY if there is an item selected
                    if(currAlbum!=null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Title");

                        // Set up the input
                        final EditText input = new EditText(MainActivity.this);
                        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                        input.setInputType(InputType.TYPE_CLASS_TEXT );
                        builder.setView(input);

                        // Set up the buttons
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                newName = input.getText().toString();
                                System.out.println("new name is: " + newName);
                                System.out.println("album: " + currAlbum);
                                renameAlbum(currAlbum, newName);
                                //not sure if this is necessary
                                thisAdapter.notifyDataSetChanged();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        builder.show();
                        //get selected album and rename it
                    }
                    //reload list if successful
                    else{
                        System.out.println("NOTHING SELECTED!!!!!!!!!!!!!!!!");
                    }
                    //albumList.setAdapter(adapter);
                } catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                }
            }
        });
        addAlbumButton.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
               try {
                   //open up dialog for name of album
                   //do it
                   AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                   builder.setTitle("CreateAlbum");

                   // Set up the input
                   final EditText input = new EditText(MainActivity.this);
                   // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                   input.setInputType(InputType.TYPE_CLASS_TEXT );
                   builder.setView(input);

                   // Set up the buttons
                   builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           newName = input.getText().toString();
                           //newAlbum=new Album(newName);
                           System.out.println("new album name is: " + newName);
                           System.out.println("album: " + currAlbum);
//                           renameAlbum(currAlbum, newName);
                           //albums.add(newAlbum);
                           addAlbum(newName);
                           //not sure if this is necessary
                           thisAdapter.notifyDataSetChanged();
                       }
                   });
                   builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           dialog.cancel();
                       }
                   });
                   AlertDialog alert = builder.create();
                   builder.show();
               } catch (NumberFormatException e) {
               }
           }
       });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //intent
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    //bundle
                    Bundle bundle = new Bundle();
                    //add list of albums to bundle
                    //bundle.putSerializable("albums", albums);
                    //attach bundle to intent

                    //intent.putExtras(bundle);
                    //switch activities to search with b
                    MainActivity.this.startActivity(intent);

                } catch (NumberFormatException e) {
                }
            }
        });
        //load the albums into the view
        ArrayAdapter<Album> adapter = new ArrayAdapter<Album>(this,R.layout.albums,albums);
        thisAdapter=adapter;
        albumList.setAdapter(thisAdapter);

    }//end of oncreate
    //serialize on stop and maybe even on pause

    protected void onStop(Bundle savedInstanceState) {
        super.onStop();
        if (VERBOSE) Log.v(TAG, "-- ON STOP --");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void onPause(Bundle savedInstanceState) {
        super.onPause();
        if (VERBOSE) Log.v(TAG, "- ON PAUSE -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (VERBOSE) Log.v(TAG, "++ ON START ++");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (VERBOSE) Log.v(TAG, "+ ON RESUME +");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (VERBOSE) Log.v(TAG, "- ON DESTROY -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Album addAlbum(String albumName){
        //if no duplicate name
        for(int i=0;i<albums.size();i++){ // if there's another album with that name, can't rename this album to it
            if(albums.get(i).getName().equalsIgnoreCase(albumName)){
                return null;
            }
        }
        //instantiate and add
        Album add = new Album(albumName);
        albums.add(add);
        return add;
    }

    /**
     * Delete album.
     *
     * @param album the album
     * @return the album
     */
    public Album deleteAlbum(Album album){
        //find
        if(albums.contains(album)){
            albums.remove(album);
            return album;
        }
        //if not found can't delete
        return null;
    }

    /**
     * Rename album.
     *
     * @param album the album
     * @param newName the new name
     * @return the album
     */
    public Album renameAlbum(Album album,  String newName){
        //find
        int i;
        for(i=0;i<albums.size();i++){ // if there's another album with that name, can't rename this album to it
            if(albums.get(i).getName().equalsIgnoreCase(newName)){
                return null;
            }
        }
        System.out.println(newName);
        //rename
        album.name =newName;
        return album ;
    }
    //public void setName(String name) {
   //     this.name = name;
    //}

}
