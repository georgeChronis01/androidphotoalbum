package com.example.george.finalphotoalbum;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;

import static android.util.Log.VERBOSE;
import static com.example.george.finalphotoalbum.MainActivity.thisApp;

public class DetailActivity extends AppCompatActivity {
    private static final String TAG = "SampleActivity";

    private static final boolean VERBOSE = true;
    private Button addTagButton;
    private Button deleteTagButton;
    private ListView tagList;
    private ImageView imgDisplay;
    private FloatingActionButton nextArrow;
    private FloatingActionButton prevArrow;
    private Tag currTag;
    private Album thisAlbum;
    private Photo thisPhoto;
    private Photo currPhoto;
    private String newTag;
    private ArrayAdapter<Tag> thisAdapter;
    public photoApp thisApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        thisApp=MainActivity.thisApp;
        addTagButton = (Button)findViewById(R.id.addTagButton);
        deleteTagButton= (Button)findViewById(R.id.deleteTagButton);
        tagList= (ListView)findViewById(R.id.tagList);
        imgDisplay= (ImageView)findViewById(R.id.imgDisplay);


        nextArrow= (FloatingActionButton)findViewById(R.id.nextArrow);
        prevArrow= (FloatingActionButton)findViewById(R.id.previousArrow);
        thisPhoto=ThumbnailActivity.currPhoto;
        currPhoto=thisPhoto;
       // imgDisplay.setImageResource();
        imgDisplay.setImageBitmap(currPhoto.display);
        // Intent intent = this.getIntent();
       // Bundle bundle = intent.getExtras();
        //load album from previous activity
        thisAlbum= ThumbnailActivity.currAlbum;

        /*
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        */

        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //need popup to enter tag

                    //probably have to check if works
                    //final String newTag;
//                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
//                    builder.setTitle("Title");
//
//                    // Set up the input
//                    final EditText input1 = new EditText(DetailActivity.this);
//                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
//                    input1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    final EditText input2 = new EditText(DetailActivity.this);
//                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
//                    input2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    builder.setView(input1);
//                    builder.setView(input2);

//                    AlertDialog.Builder dialog = new AlertDialog.Builder(imgDisplay.getContext());
//                    dialog.setTitle("Set Target Title & Description");
//                    dialog.setMessage("Title: ");
//                    Context context = imgDisplay.getContext();
//                    LinearLayout layout = new LinearLayout(context);
//                    layout.setOrientation(LinearLayout.VERTICAL);
//
//                    final EditText titleBox = new EditText(context);
//                    titleBox.setHint("Title");
//                    layout.addView(titleBox);
//
//                    final EditText descriptionBox = new EditText(context);
//                    descriptionBox.setHint("Description");
//                    layout.addView(descriptionBox);
//                    dialog.setView(layout);
                    LayoutInflater factory = LayoutInflater.from(DetailActivity.this);

                    //text_entry is an Layout XML file containing two text field to display in alert dialog
                    final View textEntryView = factory.inflate(R.layout.text_entry, null);

                    final EditText input1 = (EditText) textEntryView.findViewById(R.id.EditText1);
                    final EditText input2 = (EditText) textEntryView.findViewById(R.id.EditText2);


                    input1.setText("Person or Location", TextView.BufferType.EDITABLE);
                    input2.setText("Value", TextView.BufferType.EDITABLE);

                    final AlertDialog.Builder alert = new AlertDialog.Builder(DetailActivity.this);
                    alert.setTitle("Enter Tag: Key-Value").setView(textEntryView).setPositiveButton("Save",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {

                                    Log.i("AlertDialog","TextEntry 1 Entered "+input1.getText().toString());
                                    Log.i("AlertDialog","TextEntry 2 Entered "+input2.getText().toString());
                                    /* User clicked OK so do some stuff */
                                    String type= input1.getText().toString().trim();
                                    String value=input2.getText().toString().trim();
                                    Tag tag= new Tag(type,value);
                                    System.out.println("type: " + type + "Value: "+value );
                                    System.out.println("new tag is: " + tag);
                                    //System.out.println("album: " + currAlbum);
                                    //renameAlbum(currAlbum, newName);
                                    boolean add=currPhoto.addTag(tag);
                                    if(add){
                                        System.out.println("TAGS ADDED " );
                                    }
                                    else{
                                        //dialog
                                        System.out.println("TAGS FAILED " );
                                    }
                                    //not sure if this is necessary
                                    thisAdapter.notifyDataSetChanged();
                                }
                            }).setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {

                                }
                            });
                    alert.show();
                } catch (NumberFormatException e){

                }
            }
        });
        deleteTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(currTag!=null) {
                        //delete current tag
                        //check if legit
                        currPhoto.removeTag(currTag);
                        //maybe reload adapter
                        thisAdapter.notifyDataSetChanged();
                    }
                    else{
                        //informative popup
                    }
                } catch (NumberFormatException e){

                }
            }
        });
        tagList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                try {
                    //se;ect the clicked on tag so it can be deleted if necessary
                    tagList.setItemChecked( 0, true );
                    currTag=(Tag)tagList.getItemAtPosition(position);
                } catch (NumberFormatException e){

                }
            }
        });
        nextArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int index =thisAlbum.getPhotos().indexOf(currPhoto);
                    //if not at the end of the line, show next photo and load its tags
                    if(index==thisAlbum.getPhotos().size()-1){
                        //nothing
                    }
                    else{
                        //get next photo
                        currPhoto=thisAlbum.getPhotos().get(index+1);
                        //put it in image view
                        imgDisplay.setImageBitmap(currPhoto.display);
                        //put tags in tag list
                       // thisAdapter.notifyDataSetChanged();
                        //......
                        ArrayAdapter<Tag> adapter = new ArrayAdapter<Tag>(DetailActivity.this,R.layout.tags,currPhoto.getTags());
                        thisAdapter=adapter;
                        tagList.setAdapter(thisAdapter);
                    }
                } catch (NumberFormatException e){

                }
            }
        });
        prevArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //if not at beginning of list, show previous photo and load its tags
                    int index =thisAlbum.getPhotos().indexOf(currPhoto);
                    //if not at the end of the line, show next photo and load its tags
                    if(index<=0){
                        //nothing
                    }
                    else{
                        //get next photo
                        currPhoto=thisAlbum.getPhotos().get(index-1);
                        //put it in image view
                        imgDisplay.setImageBitmap(currPhoto.display);
                        //put tags in tag list
                        //this doesn't make sense
                       //thisAdapter.notifyDataSetChanged();
                        //......
                        ArrayAdapter<Tag> adapter = new ArrayAdapter<Tag>(DetailActivity.this,R.layout.tags,currPhoto.getTags());
                        thisAdapter=adapter;
                        tagList.setAdapter(thisAdapter);
                    }
                } catch (NumberFormatException e){

                }
            }
        });
        ArrayAdapter<Tag> adapter = new ArrayAdapter<Tag>(this,R.layout.tags,currPhoto.getTags());
        thisAdapter=adapter;
        tagList.setAdapter(thisAdapter);
    }

    protected void onStop(Bundle savedInstanceState) {
        if (VERBOSE) Log.v(TAG, "- ON stop -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    protected void onPause(Bundle savedInstanceState) {
        try {
            if (VERBOSE) Log.v(TAG, "- ON PAuse -");
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (VERBOSE) Log.v(TAG, "++ ON START ++");
    }

    @Override
    public void onResume() {

        super.onResume();

        if (VERBOSE) Log.v(TAG, "+ ON RESUME +");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (VERBOSE) Log.v(TAG, "- ON DESTROY -");
        currPhoto=null;
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, ThumbnailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               // Intent intent = new Intent(MainActivity.this, ThumbnailActivity.class);
                //probably have to pass the current album in a bundle
                Bundle bundle = new Bundle();

                //bundle.putSerializable("current album", currAlbum);
                bundle.putString("activity","detail");
                //System.out.println("===================" + currAlbum);
                intent.putExtras(bundle);

                //intent.putExtra("name_of_extra", (Serializable)currAlbum);
               // startActivity(intent);
                //Bundle bundle =
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    */


}
