
package com.example.george.finalphotoalbum;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.Serializable;

/**
 * @author George Chronis Emmanuel Ankrah
 */
import java.io.File;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;

import static android.R.attr.bitmap;
import static android.content.ContentValues.TAG;


// TODO: Auto-generated Javadoc
/**
 * The Class Photo.
 */
public class Photo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The main image to be displayed in slideshow. */
	private ImageView mainImage;

	/** The thumbnail version of image. */
	//private  ImageView thumbnail;

	/** The filename. */
	private String filename;

	/** The file. */
	private File file;

	/** The tags. */
	private ArrayList <Tag> tags;

	/** The calendar. */
	private Calendar calendar;

	/** The caption. */
	private String caption;
	public  Uri url;
	public transient Bitmap bitmap;
	/** The valid. */
	private boolean valid=true;
	//public Bitmap origin;
	public transient Bitmap thumbNail;
	public transient Bitmap display;
	//private ImageView imgView;
	public Integer resource;
	/**
	 * Instantiates a new photo.
	 *
	 * @param url the url
	 */
	public Photo(Uri uri, String url, Bitmap origin){
		this.url=uri;
		//this.file = new File(url);
        //this.setMainImage(new Image(file.toURI().toString(), 274, 155, true, true));
        //this.setThumbnail(new Image(file.toURI().toString(), 55, 40, true, true));
//		this.setMainImage(new Image(file.toURI().toString(), 274, 155, true, true));
//		this.setThumbnail(new Image(file.toURI().toString(), 55, 40, true, true));
		this.setFilename(url);
		setTags(new ArrayList<Tag>());
       // this.mainImage=new ImageView(ThumbnailActivity);
		//need to worry about milliseconds?
		Calendar cal = Calendar.getInstance();
		// cal.setTimeInMillis(0);
		 cal.set(Calendar.MILLISECOND,0);
		 this.setDate(cal);
		this.getDate();
		this.setCaption(null);
		//File imgFile = new  File("/sdcard/Images/test_image.jpg");

		//if(file.exists()){
			//System.out.println("file exists! " + file.getName());

			//Bitmap origin = BitmapFactory.decodeFile(file.getAbsolutePath());


			if(origin == null){
				System.out.println("Origin is null!");
			}
			//int nh = (int) ( origin.getHeight() * (512.0 / origin.getWidth()) );
			thumbNail = Bitmap.createScaledBitmap(origin, 300, 200, true);
			//nh = (int) ( origin.getHeight() * (200.0 / origin.getWidth()) );
			display = Bitmap.createScaledBitmap(origin, 600, 500, true);
			//
			//Bitmap d = new BitmapDrawable(ctx.getResources() , w.photo.getAbsolutePath()).getBitmap();
			//File curFile = new File("path-to-file"); // ... This is an image file from my device.
			Bitmap rotatedBitmap;


			/*
			try {
				ExifInterface exif = new ExifInterface(file.getPath());
				int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				int rotationInDegrees = exifToDegrees(rotation);
				Matrix matrix = new Matrix();
				if (rotation != 0f) {matrix.preRotate(rotationInDegrees);}
				rotatedBitmap = Bitmap.createBitmap(thumbNail,0,0, thumbNail.getWidth(), thumbNail.getHeight(), matrix, true);
				thumbNail= Bitmap.createScaledBitmap(rotatedBitmap, 300, 300, true);
				display = Bitmap.createScaledBitmap(thumbNail, 700, 700, true);

			}catch(IOException ex){
				Log.e(TAG, "Failed to get Exif data", ex);
			}
*/
		//}

	}
	public Photo(Integer resource){
		this.resource=resource;
	}


	/**
	 * Gets the image from directory when program starts because image class is not serializable.
	 *
	 * @return the image from dir
	 */
	//can't serialize images, have to recreate every time you log in
	public Bitmap getImageFromDir (Bitmap origin){
		//make file, check if null
		//File file = new File(this.filename);
//		if(file.exists()){
			 //origin = BitmapFactory.decodeFile(file.getAbsolutePath());
			//int nh = (int) ( origin.getHeight() * (512.0 / origin.getWidth()) );
			thumbNail = Bitmap.createScaledBitmap(origin, 300, 200, true);
			//nh = (int) ( origin.getHeight() * (200.0 / origin.getWidth()) );
			display = Bitmap.createScaledBitmap(origin, 600, 500, true);
			//
			//Bitmap d = new BitmapDrawable(ctx.getResources() , w.photo.getAbsolutePath()).getBitmap();
			//File curFile = new File("path-to-file"); // ... This is an image file from my device.
			Bitmap rotatedBitmap;

/*
			try {
				ExifInterface exif = new ExifInterface(file.getPath());
				int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				int rotationInDegrees = exifToDegrees(rotation);
				Matrix matrix = new Matrix();
				if (rotation != 0f) {matrix.preRotate(rotationInDegrees);}
				rotatedBitmap = Bitmap.createBitmap(thumbNail,0,0, thumbNail.getWidth(), thumbNail.getHeight(), matrix, true);
				thumbNail= Bitmap.createScaledBitmap(rotatedBitmap, 300, 300, true);
				display = Bitmap.createScaledBitmap(thumbNail, 700, 700, true);

			}catch(IOException ex){
				Log.e(TAG, "Failed to get Exif data", ex);
			}
*/
		//}
		return null;
	}

	/**
	 * Resize image.
	 *
	 * @param width the width
	 * @param height the height
	 */
//	public void resize(int width, int height){
//		//throw out current image, initialize
//		this.mainImage=new Image(this.file.toURI().toString(), width, height, true, true);
//	}

	/**
	 * Adds the tag.
	 *
	 * @param tag the tag
	 * @return true, if successful
	 */
	public boolean addTag(Tag tag){
		//if not a duplicate add
		System.out.println("addTag: "+tag);
		for(int i =0; i<this.tags.size();i++){
			System.out.println("search");
			Tag rem=null;
			if(tags.get(i).getType().equals(tag.getType())&&tags.get(i).getValue().equals(tag.getValue())){
				return false;
			}

		}
		//only two types of tags allowed for this project
		if(tag.getType().equalsIgnoreCase("location")||tag.getType().equalsIgnoreCase("person")||tag.getType().equalsIgnoreCase("Location")||tag.getType().equalsIgnoreCase("Person")){
			tags.add(tag);
			return true;
		}
		System.out.println("Not a duplicate but type doesn't match location or person");
		return false;
	}

/**
 * Removes the tag.
 *
 * @param tag the tag
 * @return the tag
 */
public Tag removeTag(Tag tag){
	//if it exists
	for(int i =0; i<this.tags.size();i++){
		Tag rem=null;
		if(tags.get(i).getType().equals(tag.getType())&&tags.get(i).getValue().equals(tag.getValue())){
			rem =tags.remove(i);
			return rem;
		}
	}
	return null;
}

	/**
	 * Gets the caption.
	 *
	 * @return the caption
	 */
	public String getCaption() {
		return caption;
	}

	/**
	 * Sets the caption.
	 *
	 * @param caption the new caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	/**
	 * Gets the main image.
	 *
	 * @return the main image
	 */
//	public Image getMainImage() {
//		return mainImage;
//	}

	/**
	 * Sets the main image.
	 *
	 * @param image the new main image
	 */
//	public void setMainImage(Image image) {
//		this.mainImage = image;
//	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public ArrayList <Tag> getTags() {
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags the new tags
	 */
	public void setTags(ArrayList <Tag> tags) {
		this.tags = tags;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Calendar getDate() {
		return calendar;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Calendar date) {
		this.calendar = date;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the thumbnail.
	 *
	 * @return the thumbnail
	 */
//	public Image getThumbnail() {
//		return thumbnail;
//	}

	/**
	 * Sets the thumbnail.
	 *
	 * @param thumbnail the new thumbnail
	 */
//	public void setThumbnail(Image thumbnail) {
//		this.thumbnail = thumbnail;
//	}

	/**
	 * Checks if filename is valid.
	 *
	 * @return true, if is valid
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Sets the valid.
	 *
	 * @param valid the new valid
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * Tags to string.
	 *
	 * @return the string
	 */
	public String tagsToString(){
		String ret="";
		for(int i =0; i<this.tags.size();i++){
			if(i==0){
				ret= tags.get(i).toString();
				continue;
			}
			ret= " , "+ tags.get(i).toString();
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.filename.toString();
	}
	private static int exifToDegrees(int exifOrientation) {
		if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
		else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
		else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
		return 0;
	}
}
