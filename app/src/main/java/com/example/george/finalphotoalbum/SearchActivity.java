package com.example.george.finalphotoalbum;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ListView;

import android.app.SearchManager;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.george.finalphotoalbum.MainActivity.thisApp;
import static com.example.george.finalphotoalbum.SearchActivity.arrayPhoto;

/**
 * Created by Rel on 12/13/2016.
 */

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SampleActivity";

    private static final boolean VERBOSE = true;
   public ArrayList<Album> theseAlbums;


    static ArrayList<Photo> disList;
    ArrayAdapter<Photo> adapter;
    static ArrayList<Photo> arrayPhoto;
    ArrayList<Photo> matches;
    ListView photoView;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        /*
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        */

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        //load album from previous activity
        //theseAlbums= (ArrayList<Album>)bundle.getSerializable("albums");
        theseAlbums=MainActivity.albums;
        System.out.println("length of albums list "+theseAlbums.size());
        System.out.println("length of photos list in first album "+theseAlbums.get(0).getPhotos().size());

        /*
        Photo test1 = new Photo("New World.jpg");
        test1.addTag(new Tag("Location", "Old Zealand"));

        Photo test2 = new Photo("new york.jpg");
        test2.addTag(new Tag("Location", "New York"));
        test2.addTag(new Tag("Person", "Emmanuel"));

        Photo test3 = new Photo("new mexico.jpg");
        test3.addTag(new Tag("Location", "New Mexico"));

        Photo test4 = new Photo("please.jpg");
        test4.addTag(new Tag("Location", "New Hampshire"));

        Photo test5 = new Photo("work.jpg");
        test5.addTag(new Tag("Location", "New Brunswick"));

*/

        photoView = (ListView)findViewById(R.id.listViewPhotos);

        //iterate through theseAlbums and put into this array
        arrayPhoto = new ArrayList<>(); // import photos

        for(int i = 0; i < theseAlbums.size(); i++){
            for(int j = 0; j < theseAlbums.get(i).getPhotos().size(); j++){
                arrayPhoto.add(theseAlbums.get(i).getPhotos().get(j));
            }
        }

        /*
        arrayPhoto.add(test1);
        arrayPhoto.add(test2);
        arrayPhoto.add(test3);
        arrayPhoto.add(test4);
        arrayPhoto.add(test5);
        */

        //System.out.println(theseAlbums.get(0).getPhotos().get(0).getTags());


        adapter = new ArrayAdapter<Photo>
                (SearchActivity.this,
                        android.R.layout.simple_list_item_1,
                        arrayPhoto);


        photoView.setAdapter(adapter);






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        // Get the SearchView and set the searchable configuration

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menuSearch).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Do not iconify the widget; expand it by default

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            public boolean onQueryTextSubmit(String query){

                ArrayAdapter<Photo> disAdapter = new ArrayAdapter<Photo>(
                        SearchActivity.this, android.R.layout.simple_list_item_1, filterAdapter(arrayPhoto, query)
                );

                photoView.setAdapter(disAdapter);

                return false;
            }

            public boolean onQueryTextChange(String newText){

                ArrayAdapter<Photo> disAdapter = new ArrayAdapter<Photo>(
                        SearchActivity.this, android.R.layout.simple_list_item_1, filterAdapter(arrayPhoto, newText)
                );

                photoView.setAdapter(disAdapter);

                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    public ArrayList<Photo> filterAdapter(ArrayList<Photo> photoList, String newText){
        ArrayList<Photo> filtered = new ArrayList<>();

        for(int i = 0; i < photoList.size(); i++){
            for(int j = 0; j < photoList.get(i).getTags().size(); j++){
                if(photoList.get(i).getTags().get(j).getValue().toLowerCase().contains(newText.toLowerCase())){
                    filtered.add(photoList.get(i));
                }
            }
        }
        return filtered;
    }
    protected void onStop(Bundle savedInstanceState) {
        if (VERBOSE) Log.v(TAG, "- ON stop -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    protected void onPause(Bundle savedInstanceState) {
        try {
            if (VERBOSE) Log.v(TAG, "- ON PAuse -");
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (VERBOSE) Log.v(TAG, "++ ON START ++");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (VERBOSE) Log.v(TAG, "+ ON RESUME +");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (VERBOSE) Log.v(TAG, "- ON DESTROY -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    */

}



