package com.example.george.finalphotoalbum;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by George on 12/12/2016.
 */


/**
 * Created by George on 12/11/2016.
 */

public class ImageViewAdapter extends BaseAdapter {
    ArrayList<ImageView> views;
    Context context;

    public ImageViewAdapter(Context context, ArrayList<ImageView> views){
        this.views = views;
        this.context = context;
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public ImageView getItem(int i) {
        return views.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = views.get(i);
        /*set the views of the rowView here but take note use
        try catch since you can't be sure if the view will be
        present or not this is the part where I do not advice
        it to have different custom views per row but it's all yours to do your tricks*/

        return rowView;
    }
}
