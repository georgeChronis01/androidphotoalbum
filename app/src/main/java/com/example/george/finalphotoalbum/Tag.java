package com.example.george.finalphotoalbum;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Tag.
 */
public class Tag implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The type. */
	//tag is a type value pair
	private String type;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new tag.
	 *
	 * @param type the type
	 * @param value the value
	 */
	public Tag(String type, String value){
		this.setType(type);
		this.setValue(value);
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type.toLowerCase();
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value.toLowerCase();
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return this.type+": " + this.value;
	}
	
}
