package com.example.george.finalphotoalbum;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import static android.util.Log.VERBOSE;

//port com.squareup.picasso.Picasso;

import static com.example.george.finalphotoalbum.MainActivity.thisApp;

public class ThumbnailActivity extends AppCompatActivity {
    private static final String TAG = "SampleActivity";

    private static final boolean VERBOSE = true;
    public static Album currAlbum;
    public static Photo currPhoto;
    public ImageView thisImgView;
    private GridView thumbView;
    private ArrayList<ImageView> theseImgViews;
    private ImageViewAdapter thisAdapter;
    private Button addPhotoButton;
    private Button deletePhotoButton;
    private Button displayPhotoButton;
    private String selectedImagePath;
    private String filemanagerstring;
    private Button movePhotoButton;
    private static final int SELECT_PICTURE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thumbnail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        */

        thumbView = (GridView) findViewById(R.id.thumbView);
        addPhotoButton = (Button)findViewById(R.id.addPhotoButton);
        deletePhotoButton = (Button)findViewById(R.id.deletePhotoButton);
        displayPhotoButton = (Button)findViewById(R.id.displayPhotoButton);
        movePhotoButton= (Button)findViewById(R.id.movePhotoButton);
        thumbView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                //view.setSelected(true);
                //trying to highlight the selected item ....
                thumbView.setItemChecked( 0, true );

                //can't cast image view to photo so just have to get the image view's path and
                //go search for photo in album, use that for currPhoto
                thisImgView=(ImageView) thumbView.getItemAtPosition(position);
               // String filename=thisImgView.getResources().getResourceName().toString();
                Bitmap bitmap = ((BitmapDrawable)thisImgView.getDrawable()).getBitmap();
                if(bitmap==null){
                    System.out.println("BITMAP IS NULL");
                }
                for(int i=0; i<currAlbum.getPhotos().size();i++) {
                    if(currAlbum.getPhotos().get(i).thumbNail==null){
                        System.out.println("get photos or get i or thumbnail is nulllllll");
                    }
                    if(currAlbum.getPhotos().get(i).thumbNail.sameAs(bitmap)){
                        currPhoto=currAlbum.getPhotos().get(i);
                    }
                }
                System.out.println(currPhoto);
//                System.out.println();
            }
        });
        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //pop up dialog...apparently have to pop up image gallery
                    //currAlbum.addPhoto();
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
//                            "content://media/internal/images/media"));
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,
                            "Select Picture"), SELECT_PICTURE);

                    //  startActivity(intent);

                    //System.out.println("THE PATH:  "+ selectedImagePath);
                    //load images
                    for(int i=0;i<currAlbum.getPhotos().size();i++){
                        System.out.println("loop0");
                        System.out.println(currAlbum.getPhotos().get(i).getFilename());
                        theseImgViews.add(new ImageView(ThumbnailActivity.this));
                        theseImgViews.get(i).setImageBitmap(currAlbum.getPhotos().get(i).thumbNail);
                        thisAdapter.notifyDataSetChanged();
                    }
                } catch (NumberFormatException e){

                }
            }
        });
        deletePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //get selected photo
                    //delete it from album and maybe the list
                    //maybe reload adapter/grid view
                    boolean delete =currAlbum.deletePhoto(currPhoto);
                    if(delete){
                        //i guess have to delete from theseImgView too
                        theseImgViews.remove(theseImgViews.indexOf(thisImgView));
                        System.out.println("successfully deleted");
                        System.out.println(" album size after delete "+currAlbum.getPhotos().size());
                        //doesn't seem to do anything here
                        thisAdapter.notifyDataSetChanged();
                        for(int i=0;i<currAlbum.getPhotos().size();i++){
                            System.out.println("delete looop");
                            System.out.println(currAlbum.getPhotos().get(i).getFilename());
                            theseImgViews.add(new ImageView(ThumbnailActivity.this));
                            theseImgViews.get(i).setImageBitmap(currAlbum.getPhotos().get(i).thumbNail);
                            theseImgViews.get(i).setPadding(8, 8, 8, 8);
                        }
                    }
                    else{
                        System.out.println("derp  delete");
                    }
                    //probably check if returns
                } catch (NumberFormatException e){

                }
            }
        });
        displayPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(currPhoto!=null) {
                        //switch activity to display/slideshow
                        //pass the photo and album....how??? or maybe just string or index of photo
                        Intent intent = new Intent(ThumbnailActivity.this, DetailActivity.class);
                        //probably have to pass the current album in a bundle
                        Bundle bundle = new Bundle();

                        //  bundle.putSerializable("current album",currAlbum);
                        //System.out.println("===================" + currAlbum);
                        //intent.putExtras(bundle);

                        //intent.putExtra("name_of_extra", (Serializable)currAlbum);
                        //startActivity(intent);
                        ThumbnailActivity.this.startActivity(intent);
                    }
                    else{

                    }
                } catch (NumberFormatException e){
                }
            }
        });
        movePhotoButton.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   try {
                       //popup dialog where you choose the album to move the photo to
                       //has to list all the albums


                       AlertDialog.Builder builderSingle = new AlertDialog.Builder(ThumbnailActivity.this);
                       //builderSingle.setIcon(R.drawable.ic_launcher);
                       builderSingle.setTitle("Select Album To Move Photo To");

                       final ArrayAdapter<Album> arrayAdapter = new ArrayAdapter<Album>(
                               ThumbnailActivity.this,
                               android.R.layout.select_dialog_singlechoice, MainActivity.albums);
                       //arrayAdapter.add(MainActivity.albums);


                       builderSingle.setNegativeButton(
                               "cancel",
                               new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialog, int which) {
                                       dialog.dismiss();
                                   }
                               });

                       builderSingle.setAdapter(
                               arrayAdapter,
                               new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialog, int which) {
                                       Album strName = arrayAdapter.getItem(which);
//                                       AlertDialog.Builder builderInner = new AlertDialog.Builder(
//                                               ThumbnailActivity.this);
//                                       builderInner.setMessage(strName.getName());
//                                       builderInner.setTitle("Your Selected Item is");
                                       //MOVE IT, put in selected album, delete from current album
                                        boolean check = strName.addPhoto(currPhoto);
                                       //might want to check if it works
                                       if(check){
                                           currAlbum.deletePhoto(currPhoto);
                                           //delete from theseImgView??
                                           //theseImgViews.remove(currPhoto);
                                           theseImgViews.remove(theseImgViews.indexOf(thisImgView));
                                           currPhoto=null;
                                           thisAdapter.notifyDataSetChanged();
                                           //shouls probably check
                                           for(int i=0;i<currAlbum.getPhotos().size();i++){
                                               System.out.println("move looop");
                                               System.out.println(currAlbum.getPhotos().get(i).getFilename());
                                               theseImgViews.add(new ImageView(ThumbnailActivity.this));
                                               theseImgViews.get(i).setImageBitmap(currAlbum.getPhotos().get(i).thumbNail);
                                               theseImgViews.get(i).setPadding(8, 8, 8, 8);
                                           }
                                           thisAdapter.notifyDataSetChanged();
                                       }
                                       //if works, delete from this album
//                                       builderInner.setPositiveButton(
//                                               "Ok",
//                                               new DialogInterface.OnClickListener() {
//                                                   @Override
//                                                   public void onClick(
//                                                           DialogInterface dialog,
//                                                           int which) {
//                                                       dialog.dismiss();
//                                                   }
//                                               });
//                                       builderInner.show();
                                   }
                               });
                       builderSingle.show();

                   }
                   catch (NumberFormatException e) {

                   }
               }
           });

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        String activity;
        //load album from previous activity
        activity= (String)bundle.get("activity");
        currAlbum=MainActivity.currAlbum;
        //i guess have to iterate through currAlbum's photos and get the url/drawable file from each
        //and make imageviews and put into array so it can go into adapter...
        if(currAlbum==null){
            System.out.println("it's null");
        }
        System.out.println("size"+currAlbum.getPhotos().size());
        //if coming from the main activity, should populate the grid view
       // if(activity.equalsIgnoreCase("main")) {
            ArrayList<ImageView> imgs = new ArrayList<ImageView>();
            theseImgViews = imgs;
            for (int i = 0; i < currAlbum.getPhotos().size(); i++) {
                //have to recreate bitmaps every time...maybe not every time
                //have to construct bitmap outside of photo class,
                //inside activity so you can get the context
                Bitmap origin = null;
                //String cast=
                Uri url = currAlbum.getPhotos().get(i).url;
                try {
                    origin = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(url));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                currAlbum.getPhotos().get(i).getImageFromDir(origin);
                System.out.println("loop1");
                System.out.println(currAlbum.getPhotos().get(i).getFilename());
                theseImgViews.add(new ImageView(ThumbnailActivity.this));
                theseImgViews.get(i).setImageBitmap(currAlbum.getPhotos().get(i).thumbNail);
                theseImgViews.get(i).setPadding(10, 10, 10, 10);
                //imgs.get(i).setImageResource(currAlbum.getPhotos().get(i).getFilename());
            }
            //put album's images inside gridview
            ImageViewAdapter adapter = new ImageViewAdapter(this, theseImgViews);
            thisAdapter = adapter;
            thumbView.setAdapter(thisAdapter);
        /*}
        else{
            System.out.println("COMING FROM DETAIL ACTIVITY");
        }*/
    }//end of oncreate

    /*
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();

                //OI FILE Manager
                filemanagerstring = selectedImageUri.getPath();

                //MEDIA GALLERY
                selectedImagePath = getPath(selectedImageUri);

                //DEBUG PURPOSE - you can delete this if you want
                if(selectedImagePath!=null)
                    System.out.println(selectedImagePath);
                else System.out.println("selectedImagePath is null");
               if(filemanagerstring!=null)
                    System.out.println(filemanagerstring);
                else System.out.println("filemanagerstring is null");

                //NOW WE HAVE OUR WANTED STRING
                if(selectedImagePath!=null){
                    System.out.println("selectedImagePath is the right one for you!");
                    System.out.println("THE PATH:  "+ selectedImagePath);
                    Photo newPhoto = new Photo(selectedImagePath);
                    if(newPhoto==null){
                        System.out.println("photo is null!!!!!!!!!!!!!!!!");
                    }
                    currAlbum.addPhoto(newPhoto);
                    System.out.println("album size after add: "+ currAlbum.getPhotos().size());
                   // notify all???
                    thisAdapter.notifyDataSetChanged();
                    for(int i=0;i<currAlbum.getPhotos().size();i++){
                        System.out.println("loop2");
                        System.out.println(currAlbum.getPhotos().get(i).getFilename());
                        theseImgViews.add(new ImageView(ThumbnailActivity.this));
                        //int id = getResources().getIdentifier(currAlbum.getPhotos().get(i).getFilename(), "drawable", getPackageName());
                        //Drawable drawable = getResources().getDrawable(id);
                        //iv.setImageDrawable(drawable);
                        //imgView = (ImageView) findViewById(R.id.imageviewTest);

                        theseImgViews.get(i).setImageBitmap(currAlbum.getPhotos().get(i).thumbNail);
                        theseImgViews.get(i).setPadding(10, 10, 10, 10);
                        //imgs.get(i).setImageResource(currAlbum.getPhotos().get(i).getFilename());
                        thisAdapter.notifyDataSetChanged();
                    }
                }
                else
                    System.out.println("filemanagerstring is the right one for you!");
            }
        }
    }

    //UPDATED!
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if(cursor!=null)
        {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        else return null;
    }

    */

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                Log.d("URI VAL", "selectedImageUri = " + selectedImageUri.toString());
                selectedImagePath = getPath(selectedImageUri);

                if(selectedImagePath!=null){
                    // IF LOCAL IMAGE, NO MATTER IF ITS DIRECTLY FROM GALLERY (EXCEPT PICASSA ALBUM),
                    // OR OI/ASTRO FILE MANAGER. EVEN DROPBOX IS SUPPORTED BY THIS BECAUSE DROPBOX DOWNLOAD THE IMAGE
                    // IN THIS FORM - file:///storage/emulated/0/Android/data/com.dropbox.android/...
                    System.out.println("local image");
                }
                else{
                    System.out.println("picasa image!");
                    loadPicasaImageFromGallery(selectedImageUri);
                    System.out.println("PICASA: " + selectedImageUri);
                    System.out.println("selectedImagePath: " + selectedImagePath);
                }
                }
            }
        }


    // NEW METHOD FOR PICASA IMAGE LOAD
    private void loadPicasaImageFromGallery(final Uri uri) {
        String[] projection = {  MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if(cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
            //cursor.close();
            if (columnIndex != -1) {
                new Thread(new Runnable() {
                    // NEW THREAD BECAUSE NETWORK REQUEST WILL BE MADE THAT WILL BE A LONG PROCESS & BLOCK UI
                    // IF CALLED IN UI THREAD
                    @Override
                    public void run() {
                        try {
                            Bitmap bitmap = android.provider.MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("URI: " + uri);

                                    Bitmap origin = null;
                                    try {
                                        origin = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(uri));
                                        //closeInputStream();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    Photo newPhoto = new Photo(uri, uri.getPath(), origin);
                                    System.out.println("newPhoto: " + newPhoto.getFilename());
                                    currAlbum.addPhoto(newPhoto);
                                    System.out.println("album size after add: "+ currAlbum.getPhotos().size());
                                    thisAdapter.notifyDataSetChanged();

                                    for(int i=0;i<currAlbum.getPhotos().size();i++){
                                        System.out.println("loop2");
                                        System.out.println(currAlbum.getPhotos().get(i).getFilename());
                                        theseImgViews.add(new ImageView(ThumbnailActivity.this));
                                        //int id = getResources().getIdentifier(currAlbum.getPhotos().get(i).getFilename(), "drawable", getPackageName());
                                        //Drawable drawable = getResources().getDrawable(id);
                                        //iv.setImageDrawable(drawable);
                                        //imgView = (ImageView) findViewById(R.id.imageviewTest);

                                        theseImgViews.get(i).setImageBitmap(currAlbum.getPhotos().get(i).thumbNail);
                                        theseImgViews.get(i).setPadding(10, 10, 10, 10);
                                        //imgs.get(i).setImageResource(currAlbum.getPhotos().get(i).getFilename());
                                        thisAdapter.notifyDataSetChanged();
                                    }

                                }
                            });

                            // THIS IS THE BITMAP IMAGE WE ARE LOOKING FOR.
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            }
        }
        cursor.close();
    }


    public String getPath(Uri uri) {
        String[] projection = {  MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if(cursor != null) {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            String filePath = cursor.getString(columnIndex);
            cursor.close();
            return filePath;
        }
        else
            return uri.getPath();               // FOR OI/ASTRO/Dropbox etc
    }

    protected void onStop(Bundle savedInstanceState) {
        if (VERBOSE) Log.v(TAG, "- ON stop -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    protected void onPause(Bundle savedInstanceState) {
        try {
            if (VERBOSE) Log.v(TAG, "- ON PAuse -");
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (VERBOSE) Log.v(TAG, "++ ON START ++");
    }

    @Override
    public void onResume() {
        super.onResume();
        currPhoto=null;
        if (VERBOSE) Log.v(TAG, "+ ON RESUME +");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        currPhoto=null;
        if (VERBOSE) Log.v(TAG, "- ON DESTROY -");
        try {
            photoApp.writeApp(this, thisApp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

*/

}
