package com.example.george.finalphotoalbum;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;



// TODO: Auto-generated Javadoc
/**
 * The Class photoApp, the highest serializable class.
 */
public class photoApp implements Serializable {
	
	/** The Constant storeDir. */
	public static final String storeDir = "Data";
	
	/** The Constant storeFile. */
	public static final String storeFile = "albums.dat";
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The users. */
	public ArrayList<Album> albums;
	
	/**
	 * Instantiates a new photo app.
	 */
	public photoApp(){
		albums =new ArrayList<Album>();
	}
	

	public static void writeApp(Context context, photoApp app)
			throws IOException{
//				ObjectOutputStream oos = new ObjectOutputStream(
//						new FileOutputStream(storeDir+File.separator+storeFile)	);
//						oos.writeObject(logCont);
//						oos.close();
		FileOutputStream fos = context.openFileOutput(storeFile, Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(app);
		os.close();
		fos.close();
			}

	public static photoApp readApp(Context context)
			throws IOException, ClassNotFoundException{
//				ObjectInputStream ois = new ObjectInputStream(
//					new FileInputStream(storeDir + File.separator + storeFile));
//				photoApp log= (photoApp)ois.readObject();
//					ois.close();
		FileInputStream fis = context.openFileInput(storeFile);
		ObjectInputStream is = new ObjectInputStream(fis);
		photoApp log = (photoApp) is.readObject();
		is.close();
		fis.close();
				return log;
			}
	

}
